CREATE TABLE url_user (
    id uuid NOT NULL,
    user_full TEXT,
    email TEXT,
    password TEXT NOT NULL,
    refresh_token TEXT,
    created_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP(0) WITH TIME zone NULL
);
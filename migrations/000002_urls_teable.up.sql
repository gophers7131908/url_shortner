CREATE TABLE url_table (
    id UUID NOT NULL,
    long_url TEXT,
    short_url TEXT
);
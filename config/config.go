package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment string // develop, staging, production
	
	CtxTimeout int
	RedisHost string
	RedisPort string

	PostgresHost string
	PostgresPort int
	PostgresUser string
	PostgresPassword string
	PostgresDB string
	DatabaseURL string
	Rouls string
	SignKey string
	AuthConfigPath string
	LogLevel string
	HTTPPort string
}

// Load loads environment vars and inflates Config
func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":9088"))

	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "sdy12197"))
	c.PostgresDB = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "reviewdb"))
	c.DatabaseURL = cast.ToString(getOrReturnDefault("DATABASE_URL", "postgres://developer:2002@localhost:5432/reviewdb"))

	c.RedisHost = cast.ToString(getOrReturnDefault("REDIS_HOST","redis"))
	c.RedisPort = cast.ToString(getOrReturnDefault("REDIS_PORT","6379"))

	c.SignKey = cast.ToString(getOrReturnDefault("SIGN_KEY", "secret"))
	c.AuthConfigPath = cast.ToString(getOrReturnDefault("AUTH_PATH", "./config/rabc_model.conf"))
	c.CtxTimeout = cast.ToInt(getOrReturnDefault("CTX_TIMEOUT", 7))



	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}

package postgres

import (
	//	"database/sql"
	//	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/url_shortner/storage/repo"
)

type urlRepo struct {
	db *sqlx.DB
}

func NewUrl(db *sqlx.DB) repo.UrlSStorageI {
	return &urlRepo{
		db: db,
	}
}

func (ur *urlRepo) Create(u *repo.User) (*repo.User, error) {
	result := repo.User{}
	id := uuid.New().String()
	u.ID=id
	qwery := `INSERT INTO url_user(
		id, 
		user_full,
		email,
		password,
		refresh_token,
		created_at,
		updated_at
	) VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING id, user_full, email, password, refresh_token, created_at, updated_at`
	row := ur.db.QueryRow(qwery, u.ID, u.User_full, u.Email, u.Password, u.Refresh_token, u.CreatedAt, u.UpdatedAt)
	err := row.Scan(&result.ID, &result.User_full, &result.Email, &result.Password, &result.Refresh_token,
		&result.CreatedAt, &result.UpdatedAt)

	if err != nil {
		return &repo.User{}, err
	}
	return &result, nil
}

func (ur *urlRepo) Get(Id string) (*repo.User, error) {
	qwery := `SELECT 
		id,
		user_full,
		email,
		password,
		refresh_token,
		created_at,
		updated_at
	FROM  url_user WHERE id=$1`

	result := repo.User{}

	row := ur.db.QueryRow(qwery, Id)
	err := row.Scan(
		&result.ID,
		&result.User_full,
		&result.Email,
		&result.Password,
		&result.CreatedAt,
		&result.UpdatedAt,
	)
	if err != nil {
		return &repo.User{}, nil
	}

	return &result, nil
}

func (ur *urlRepo) GetByEmail(email string) (*repo.User, error) {
	qwery := `SELECT 
	id,
	user_full,
	email,
	password,
	refresh_token,
	created_at,
	updated_at
	FROM  url_user WHERE email=$1`

	result := repo.User{}

	row := ur.db.QueryRow(qwery, email)
	err := row.Scan(
		&result.ID,
		&result.User_full,
		&result.Email,
		&result.Password,
		&result.CreatedAt,
		&result.UpdatedAt,
	)
	if err != nil {
		return &repo.User{}, nil
	}
	return &result, nil
}

func (ur *urlRepo) Url_insert(rl *repo.Link) (*repo.Link, error) {
	result:=repo.Link{}
	id := uuid.New()
	qwery := `INSERT INTO url_table(
		id,
		long_url,
    	short_url
	) VALUES($1, $2, $3) RETURNING short_url`
	newURL := "asd"
	row := ur.db.QueryRow(qwery, id, rl.Url, newURL)
	err := row.Scan(&result.Url)
	if err != nil {
		return &repo.Link{}, err
	}
	return &result, nil
}

func (ur *urlRepo) Url_select_long(rl *repo.Link) (*repo.Link, error) {
	result:=repo.Link{}
	qwery:= `SELECT short_url FROM url_table WHERE long_url=$1`
	row:= ur.db.QueryRow(qwery, rl.Url)
	err := row.Scan(&result.Url)
	if err != nil {
		return &repo.Link{}, err
	}

	return &result, nil
} 

func (ur *urlRepo) Url_select_short(rl *repo.Link) (*repo.Link, error) {
	result:=repo.Link{}
	qwery:= `SELECT short_url FROM url_table WHERE long_url=$1`
	row:= ur.db.QueryRow(qwery, rl.Url)
	err := row.Scan(&result.Url)
	if err != nil {
		return &repo.Link{}, err
	}

	return &result, nil
} 

func (ur *urlRepo) Url_delete(rl *repo.Link) (error) {
	qwery:= `DELETE FROM url_table WHERE short_url=$1`
	_ = ur.db.QueryRow(qwery, rl.Url)

	return nil
}


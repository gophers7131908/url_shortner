package storage

import (

	"github.com/jmoiron/sqlx"
	"gitlab.com/url_shortner/storage/postgres"
	"gitlab.com/url_shortner/storage/repo"
)

type StorageI interface {
	Url() repo.UrlSStorageI
}

type storagePg struct {
	urlRepo repo.UrlSStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		urlRepo:     postgres.NewUrl(db),
	}
}

func (s *storagePg) Url() repo.UrlSStorageI {
	return s.urlRepo
}
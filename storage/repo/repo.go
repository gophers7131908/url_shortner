package repo

import "time"

type User struct {
	ID        string
	User_full string
	Email     string
	Password  string
	Refresh_token string
	CreatedAt time.Time
	UpdatedAt time.Time
}

//id uuid NOT NULL,
//    user_full TEXT,
//    email TEXT,
//    password TEXT NOT NULL,
//    refresh_token TEXT,
//    created_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
//    updated_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
//    deleted_at TIMESTAMP(0) WITH TIME zone NULL
type Link struct {
	Url string
}

type UrlSStorageI interface {
	Create(u *User) (*User, error)
	Get(id string) (*User, error)
	GetByEmail(email string) (*User, error)
	Url_insert(*Link) (*Link, error)
	Url_select_short(*Link) (*Link, error)
	Url_select_long(*Link) (*Link, error)
	Url_delete(*Link) (error)
}
